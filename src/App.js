import React from "react";
import { BrowserRouter, Link, NavLink, Switch, Route } from "react-router-dom";
import { Container, Row, Col, Button } from 'reactstrap';

// importem components
import Inici from './components/Inici';
import Taula from './components/Taula';
import NavMenu from './components/NavMenu';
import Edita from './components/Edita';
import P404 from './components/P404';
import ListaPokemons from './components/ListaPokemons';
import DetallPokemon from './components/DetallPokemon';

//importem models
import Usuari from './models/Usuari';
import Projecte from './models/Projecte';
import Tasca from './models/Tasca';

// importamos css
import 'bootstrap/dist/css/bootstrap.min.css';
import 'font-awesome/css/font-awesome.min.css';
import './app.css';


//xmysql -h localhost -u root -p admin -d codesplai

// clase App 
export default class App extends React.Component {


  render() {

    return (
      <BrowserRouter>

        <NavMenu />

        <Container>

          <Row>
            
            <Col>
              <Switch>
                <Route exact path="/" component={Inici} />

                <Route exact path="/usuaris" render={() => <Taula model={Usuari} />} />
                <Route path="/usuaris/nou" render={() => <Edita model={Usuari} />} />
                <Route path="/usuaris/edita/:id" render={(props) => <Edita {...props} model={Usuari} />} />

                <Route exact path="/projectes" render={() => <Taula model={Projecte} />} />
                <Route path="/projectes/nou" render={() => <Edita model={Projecte} />} />
                <Route path="/projectes/edita/:id" render={(props) => <Edita {...props} model={Projecte} />} />

                <Route exact path="/tasques" render={() => <Taula model={Tasca} />} />
                <Route path="/tasques/nou" render={() => <Edita model={Tasca} />} />
                <Route path="/tasques/edita/:id" render={(props) => <Edita {...props} model={Tasca} />} />

                <Route exact path="/pokemons" render={() => <ListaPokemons />} />
                <Route exact path="/pokemons/:id" render={(props) => <DetallPokemon  {...props} />} />
                
                
                <Route component={P404} />
              </Switch>
            </Col>
          </Row>

        </Container>
      </BrowserRouter>
    );

  }

}
