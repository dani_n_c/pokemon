import React from "react";

import { Button, Table } from 'reactstrap';
import DataModal from './DataModal.jsx';
import { Link } from 'react-router-dom';

export default class Lista extends React.Component {


    constructor(props) {
        super(props);
        this.state = {
            data: [],
            itemToDelete: 0,
            loadedModel: '',

            modalAction: null,
            modalVisible: false,
            modalTitle: '',
            modalBody: '',
            modalBtnOk: '',
            modalBtnNok: '',

        }

        this.delete = this.delete.bind(this);
        this.executeDelete = this.executeDelete.bind(this);
        this.getData = this.getData.bind(this);
        this.getData();
    }


    //obté dades del model rebut via props
    getData() {
        this.props.model.getAll()
            .then(data => this.setState({ data }))
            .then(()=>this.setState({loadedModel: this.props.model.getModelName()}))
            .catch(err => console.log(err));
    }

   
    //configura i mostra modal de confirmació per eliminar
    // veure DataModal.jsx
    delete(id) {
        this.setState({ 
            modalVisible: true, 
            modalAction: this.executeDelete,
            modalParameter: id,
            modalTitle: 'Confirmació',
            modalBody: 'Eliminar registre?',
            modalBtnOk: 'Eliminar',
            modalBtnNok: 'Cancel.lar' });
    }

    //si es clica "ok" en el modal de confirmació s'executa el "modalAction" que apunta aquest mètode
    //aquí és on s'esborra realment el registre, només si "resp" és "ok"
    executeDelete(resp, id) {
        if (resp === 'ok') {
            let novaData = this.state.data.filter(el => el.id !== id);
            this.setState({ data: novaData });
            this.props.model.deleteById(id);
        }
        this.setState({ modalVisible: false });
    }


    render() {
        // com que utilitzem un mateix component, cal verificar si dades carregades 
        // son les del model solicitat
        // la primera vegada sempre serà així
        if (this.state.loadedModel!==this.props.model.getModelName()){
            this.getData();
            return <></>;
        }
        
    
        let fieldNames = this.props.model.getFields();
        // titols de les columnes a partir dels camps del model
        let titolsColumnes = fieldNames.map(el =>  <th key={el.name}>{el.name}</th>);
  
        //creem les files a partir de dades rebudes, ordenades per id
        let filas = this.state.data.sort((a, b) => a.id - b.id).map(item => {
                let cols = fieldNames.map(el => <td key={el.name}>{item[el.name]}</td>)
                return (
                    <tr key={item.id}>
                        <td >{item.id}</td>
                        {cols}
                        <td><Link  to={`/${this.props.model.getCollection()}/edita/${item.id}`}><i  className="fa fa-edit clicable blue-icon" /></Link></td>
                        <td><i onClick={() => this.delete(item.id)} className="fa fa-trash clicable red-icon" /></td>
                    </tr>
                );
            })
        

        return (
            <>
                <h1 className="titol-vista">{this.props.model.getTitle()}</h1>
               <Table>
                    <thead>
                        <tr>
                            <th>#</th>
                            {titolsColumnes}
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {filas}
                    </tbody>
                </Table>
                <br />
                <Link className="btn btn-primary" to={`/${this.props.model.getCollection()}/nou`} >Nou registre</Link>

                <DataModal visible={this.state.modalVisible}
                    title={this.state.modalTitle}
                    body={this.state.modalBody}
                    okText={this.state.modalBtnOk}
                    nokText={this.state.modalBtnNok}
                    action={this.state.modalAction}
                    parameter={this.state.modalParameter}
                     />

            </>
        );
    }


}

