import React from "react";

import { Button, Table } from 'reactstrap';
import { Redirect, Link } from 'react-router-dom';

const  IconoPokemon = (props) => <img src={"https://img.pokemondb.net/sprites/omega-ruby-alpha-sapphire/dex/normal/"+props.nombre.toLowerCase()+".png"} />;
const IconoPokemonUltra= (props) => <img src={"https://img.pokemondb.net/sprites/ultra-sun-ultra-moon/small/"+props.nombre.toLowerCase()+".jpg"}/>;
let pagina=8;

export default class ListaPokemons extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            pokemons: [],
        }
        this.getData = this.getData.bind(this);
        this.getData();
    }

    getData() {
        const fetchURL = "http://localhost:3000/api/pokemons?_p="+pagina+"&_size=100";
        fetch(fetchURL)
            .then(results => results.json())
            .then(data => this.setState({ pokemons: data }))
            .catch(err => console.log(err));
        pagina= pagina + 1; 
       
    }
    

    render() {

        if (this.state.pokemons.length === 0) {
            return <>...</>;
        }
     
        //creem les files a partir de dades rebudes, ordenades per id
        
        let filas = this.state.pokemons.map(item => {
            
             let icono =(item.id<=721 ?  <IconoPokemon nombre={item.nombre}/> : <IconoPokemonUltra nombre={item.nombre}/>);
            return (
                <tr key={item.id}>
                    <td >{item.id}</td>
                    <td>{icono}</td>
                    <td>{item.nombre}</td>
                    <td>{item.tipo}</td>
                    <td><Link to={`/pokemons/${item.id}`} className="btn btn-primary">Detall</Link></td>
             
                </tr>
            );
        })

        return (
            <>
                <h1 className="titol-vista">Pokemons</h1>
                <Table>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th></th>
                            <th>Nombre</th>
                            <th>Tipo</th>
                        </tr>
                    </thead>
                    <tbody>
                        {filas}
                    </tbody>
                </Table>
                
            </>
        );
    }

}

