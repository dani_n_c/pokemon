import React from "react";
import { Redirect } from 'react-router-dom';

import { Button, Form, FormGroup, Label, Input } from 'reactstrap';



const FotoPokemon = (props) => <img width="100px" src={"https://img.pokemondb.net/artwork/large/" + props.nombre.toLowerCase() + ".jpg"} />;
export default class DetallPokemon extends React.Component {
    constructor(props) {
        super(props);

        //si no rebem paràmetre considerem item nou
        let id = 0;
        if (this.props.match && this.props.match.params.id) {
            id = this.props.match.params.id * 1;
        }

        this.state = {
            tornar: false,

        }

        this.getElement = this.getElement.bind(this);
        this.torna = this.torna.bind(this);

        if (id > 0) {
            this.carregaRegistre(id);
        }
    }

    getElement() {
        const fetchURL = "http://localhost:3000/api/pokemons/" + id;
        fetch(fetchURL)
            .then(results => results.json())
            .then(data => this.setState(data[0]))
            .catch(err => console.log(err));
    }
    torna() {
        this.setState({ tornar: true });
    }

    render() {


        if (this.state.nombre === undefined) {
            return <></>;
        }

        if (this.state.tornar === true) {
            return <Redirect to={'/pokemons'} />
        }


        return (
            <div className="text-center" >
                <h1 className="titol-vista">{"Pokemon " + this.state.nombre}</h1>
                <FotoPokemon nombre={this.state.nombre} />
                <br />
                <br />
                <Button color="primary" onClick={this.torna}>Volver</Button>

            </div>
        );
    }
}