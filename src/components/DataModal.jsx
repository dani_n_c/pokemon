import React, { Component } from 'react';
import {  Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

// dataModal mostra un modal de bootstrap, amb un botó OK i un botó CANCEL
// tota la informació a mostrar arriba via props:
// props.title: titol del modal
// props.body: text a mostrar
// props.okText: text al boto "ok", si no es rep res no es mostra el botó
// props.nokText: text al botó "cancel", si no es rep res es mostra un botó "sortir"
// props.parameter: paràmetre que es tornarà com a resposta
// props.action: acció de resposta, se li passaràn els paràmetres ok/nok i el paràmetre rebut

class DataModal extends Component {
    
    render() {
        
        let nokText = (this.props.nokText) ? this.props.nokText : "Sortir";

        let boto1 = (this.props.okText) ? <Button color="primary" onClick={()=>this.props.action('ok', this.props.parameter)}>{this.props.okText}</Button> : <span></span>;
        let boto2 = <Button color="secondary" onClick={()=>this.props.action('nok', this.props.parameter)}>{nokText}</Button>;
  
        return (
            <Modal isOpen={this.props.visible} >
            <ModalHeader>{this.props.title}</ModalHeader>
            <ModalBody>
                {this.props.body}
            </ModalBody>
            <ModalFooter>
                {boto1}
                {boto2}
            </ModalFooter>
          </Modal>
        );
    }
}


export default DataModal;